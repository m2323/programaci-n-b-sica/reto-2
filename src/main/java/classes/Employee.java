
package classes;

/**
 * This class contains a bascis attributes and methods to create a basic employee and calculate your salary
 * @author jcebalus
 * @version 1.0
 */
public class Employee {
    
    private String name;
    private String last_name;
    private int age;
    private float total_salary;

    /**
     * This method return employee's name
     * @return 
     */
    public String getName() {
        return name;
    }
    
    /**
     * This method establish employee's name
     * @param name 
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * This method return employee's last name
     * @return 
     */
    public String getLast_name() {
        return last_name;
    }
    /**
     * This method establish employee's last name
     * @param last_name 
     */
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }
    /**
     * This method return employee's age
     * @return 
     */
    public int getAge() {
        return age;
    }
    /**
     * This method establish employee's age
     * @param age 
     */
    public void setAge(int age) {
        this.age = age;
    }
    /**
     * This method calculate the total salary of the employee
     * @param base_salary
     * @param bonuses
     * @param loans 
     */
    public void calculateTotalSalary(float base_salary, float bonuses, float loans){
        this.total_salary = (base_salary + bonuses) - loans;
        System.out.println("Name: "+this.name+"/tAge: "+this.age+"\tTotal salary: $"+this.total_salary);
    }
}
