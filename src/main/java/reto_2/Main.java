/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reto_2;
import java.util.Scanner;
import classes.Employee;

/**
 *
 * @author jcebalus
 */
public class Main {
    public static void main(String[] args) {
        
        float base_salary;
        float bonuses;
        float loans;
        
        Scanner scan = new Scanner(System.in);
        Employee employee = new Employee();
        
        System.out.println("Name:");
        employee.setName(scan.next());
        System.out.println("Last name:");
        employee.setLast_name(scan.next());
        System.out.println("Age:");
        employee.setAge(scan.nextInt());
        
        System.out.println("Base salary:");
        base_salary = scan.nextFloat();
        System.out.println("Bonuses:");
        bonuses = scan.nextFloat();
        System.out.println("Loans:");
        loans = scan.nextFloat();
        
        employee.calculateTotalSalary(base_salary, bonuses, loans);
    }
}
